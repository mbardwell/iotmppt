import Adafruit_BBIO.ADC as ADC
import Adafruit_BBIO.GPIO as GPIO
from Adafruit_I2C import Adafruit_I2C
import time
import smbus
import os
import sys

#Initialise
duty_MIN = 0x00
duty_MAX = 0x40  # assuming 12 bit duty
duty_STEP = 0x01 # 64 possible steps with DAC
increase_duty = 0
current_duty = 0x00 # Start with a small amount of current so FET doesn't heat

# Run sudo check
if os.getuid() == 0:
    print('Running in sudo')
else:
    print('Error: Run in sudo')
    exit()

# ADC setup
ADC.setup(); print('ADC setup complete')
GPIO.setup("USR0", GPIO.OUT); print('GPIO setup complete')

# Read panel voltage
def vsense():
    value = ADC.read("AIN1")
    voltage = value * 1.8 #1.8V
    return voltage

# Read panel current
def isense():
    current = vsense()/2.28
    # k_curr = 1
    # value = ADC.read("AIN2")
    # current = value * 1.8 * k_curr
    return current

def vref(ref):
	data1 = [0x00, ref]  #0x00 : command code -- 0x40 : wiper value (specifies rat$
	bus = smbus.SMBus(2)
	temp = bus.write_i2c_block_data(0x2E, 0x00, data1)

# def po_duty_keep_direction(float p, float p_last):
#   tol = 0.001
#   if (p - p_last > tol):
#     return true
#   else
#     return false

# def po_duty(current_duty): # Added pass-through variable
#   if(!po_duty_keep_direction(P_reading, last_P_reading)):
#     increase_duty ^= 1 # What is ^= ?
#
#   if(increase_duty):
#     current_duty = min((current_duty + duty_STEP), duty_MAX)
#
#   else
#     current_duty = max((current_duty - duty_STEP), duty_MIN)
#
# def ic_duty(current_duty): # Added pass-through variable
#   dV = V_reading - last_V_reading
#   dI = I_reading - last_I_reading
#   dP = P_reading - last_P_reading
#   tol = 0.001
#
#   if (round(dV) < tol & round(dV) > -tol ): # was round(dV)==0
#     if (round(dI) > tol | round(dI) < -tol): # was round(dI)!=0
#       if (dI > 0) :
#         current_duty = min((current_duty + duty_STEP), duty_MAX);
#       else
#         current_duty = max((current_duty - duty_STEP), duty_MIN);
#   else
#     if (round(dP/dV) > tol | round(dP/dV) < -tol): # was round(dP/dV) != 0
#       if(dP/dV > 0):
#         current_duty = min((current_duty + duty_STEP), duty_MAX)
#       else
#         current_duty = max((current_duty - duty_STEP), duty_MIN);

# def FOCV_duty(current_duty):
#   float Koc = 0.8;
#   v_mpp = Koc*measure_voc();
#   current_duty = duty_setting_function(v_mpp)

argv = sys.argv[1] # Command line argument
vref(current_duty) # Initialise FET
voltage = vsense()
if voltage > int(0x02,16); print('Error: FET not off'); exit()

if __name__ == "__main__":
    while True:
        GPIO.output("USR0", GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output("USR0", GPIO.LOW)
        time.sleep(0.5)
