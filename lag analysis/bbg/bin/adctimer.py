import Adafruit_BBIO.ADC as ADC
import Adafruit_BBIO.GPIO as GPIO
import time
import os
import numpy as np

if os.getuid() == 0:
    print('Running in sudo')
else:
    print('Error: Run in sudo')
    exit()

ADC.setup()

samplemean = range(1,100)
start = range(1,1000); end = range(1,1000)
while True:
    for j in range(0,99):
        for i in range(0,999):
            start[i] = time.clock()
            value0 = ADC.read("AIN0")
            end[i] = time.clock()
        samplemean[j] = (sum(end)-sum(start))/len(end)
    stddev = np.std(samplemean)
    normalmean = sum(samplemean)/len(samplemean)
    print(normalmean, stddev); exit()
