import time
import timeit
import Adafruit_BBIO.ADC as ADC
import Adafruit_BBIO.GPIO as GPIO

ADC.setup(); print('ADC setup successful')

start = time.time()
value = ADC.read("AIN1");
end = time.time()
print(value)
print(end-start)
