import Adafruit_BBIO.ADC as ADC 
import Adafruit_BBIO.GPIO as GPIO 
from Adafruit_I2C import Adafruit_I2C 
import time 
import smbus 
import os

if os.getuid() == 0:
    print('Running in sudo')
else:
    print('Error: Run in sudo')	
    exit()

ADC.setup()
GPIO.setup("USR0", GPIO.OUT)
a = 0x20

def vref(ref):
	data1 = [0x00, ref]  #0x00 : command code -- 0x40 : wiper value (specifies rat$
	bus = smbus.SMBus(2)
	temp = bus.write_i2c_block_data(0x2E, 0x00, data1)
vref(a)

while True:
        value = ADC.read("AIN1")
        voltage = value * 1.8 #1.8V
        print(voltage)
        GPIO.output("USR0", GPIO.HIGH)
        time.sleep(0.5)
        GPIO.output("USR0", GPIO.LOW)
        time.sleep(0.5)
