from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import numpy as np

hostpath = open("/home/debian/AWS/bbgSDK/restAPI.txt", "r")
host = hostpath.read()
host = host[:-1] # Eliminate newline character
clientId = 'BBBattempt2'
rootCAPath = '/home/debian/AWS/bbgSDK/rootCA.crt'
certificatePath = '/home/debian/AWS/bbgSDK/8a8d32104c-certificate.pem.crt'
privateKeyPath = '/home/debian/AWS/bbgSDK/8a8d32104c-private.pem.key'
topic = 'awstimetrials'

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None
myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
myAWSIoTMQTTClient.configureEndpoint(host, 8883)
myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)
#myAWSIoTMQTTClient.disableMetricsCollection()

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()
# time.sleep(2)
print("Connected to client")

# Publish function
def pub(content):
    message = {}
    message['message'] = content
    messageJson = json.dumps(message)
    myAWSIoTMQTTClient.publish(topic, messageJson, 1)

count, subcount = 0, 0
end, start = range(100), range(100)
samplemean = range(30)
def customCallback(client, userdata, message):
    global count
    pubtime = message.payload[12:17]
    start[count] = float(pubtime)
    end[count] = time.clock()
    count += 1
    if (count == 100):
	global subcount
        samplemean[subcount] = (sum(end)-sum(start))/len(end)
	subcount += 1
	count = 0
	if (subcount == 30):
	    normalmean = sum(samplemean)/len(samplemean)
	    sigma = np.std(samplemean)
	    print(normalmean, "sigma: ", sigma)

def sub():
    myAWSIoTMQTTClient.subscribe(topic, 1, customCallback)
sub()

def rec(flag):
    time = time

# Publish to the same topic in a loop forever
while True:
    pub(time.clock())
    # time.sleep(1)
