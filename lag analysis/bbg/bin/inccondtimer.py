import time
import os
import sys
import random as rand
import numpy as np

# Run sudo check
if os.getuid() == 0:
    print('Running in sudo')
else:
    print('Error: Run in sudo')
    exit()

def get_power(v,i):
    return v*i

def ic_duty(duty):
    dV = v_reading - v_last
    dI = i_reading - i_last
    dP = p_reading - p_last
    # print("%0.4f"%(dP), "%0.4f"%(dI), "%0.4f"%(dV), "%0.4f"%(duty))
    if dV == 0:
        if dI != 0:
            if dI > 0:
                duty = min(duty+step,1)
            else:
                duty = max(duty-step,0)
    else:
        if dP/dV != 0:
            if dP/dV > 0:
                duty = min(duty+step,1)
            else:
                duty = max(duty-step,0)
    return duty

p_last = 0; v_last = 0; i_last = 0
duty = 0.5; step = 0.0025
samplemean = range(1,100); start = range(1,1000); end = range(1,1000)
while True:
    for j in range(0,99):
        for i in range(0,999):
            v_reading = rand.randrange(0, 3)
            i_reading = rand.randrange(0, 3)
            start[i] = time.time()
            p_reading = get_power(v_reading, i_reading)
            duty = ic_duty(duty)
            v_last = v_reading
            i_last = i_reading
            p_last = p_reading
            end[i] = time.time()
        samplemean[j] = (sum(end)-sum(start))/len(end)
    stddev = np.std(samplemean)
    normalmean = sum(samplemean)/len(samplemean)
    print(normalmean, stddev); exit()
    print((sum(end) - sum(start))/len(end)); exit()
    #time.sleep(0.5)
