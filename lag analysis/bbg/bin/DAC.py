import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.PWM as PWM
from Adafruit_I2C import Adafruit_I2C
import smbus
import time

data1 = [0x00, 0x01]  #0x00 : command code -- 0x40 : wiper value (specifies ratio /0x40)
data2 = [0x00, 0x00]

bus = smbus.SMBus(2)
temp = bus.write_i2c_block_data(0x2E, 0x00, data1)
print(temp)

time.sleep(5)

temp = bus.write_i2c_block_data(0x2E, 0x00, data2)
print(temp)
