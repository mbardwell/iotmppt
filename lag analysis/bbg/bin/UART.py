# BBG has six serial ports
# Only /dev/ttyOo is enabled by default (others require device tree overlays mods)

import Adafruit_BBIO.UART as UART
import serial

UART.setup("UART2")

with serial.Serial(port = "/dev/tty01", baudrate = 9600, timeout=1) as ser:
     x = ser.read()          # read one byte
     s = ser.read(10)        # read up to ten bytes (timeout)
     line = ser.readline()   # read a '\n' terminated line
     print(x, s, line)
