import Adafruit_BBIO.GPIO as GPIO
import Adafruit_BBIO.PWM as PWM
from Adafruit_I2C import Adafruit_I2C
import smbus
import time

data = 0x01

bus = smbus.SMBus(2)
start = range(1,10);end = range(1,10)

for i in range(0,9):
    start[i] = time.time()
    temp  = bus.write_i2c_block_data(0x2E, 0x00, [0x00, data])
    end[i] = time.time()
avg = sum(end) - sum(start)
print(avg/len(end))    
