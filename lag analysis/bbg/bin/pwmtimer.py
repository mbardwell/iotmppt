import Adafruit_BBIO.PWM as PWM
import time
import os
import random as rand
import numpy as np

if os.getuid() == 0:
    print('Running in sudo')
else:
    print('Error: Run in sudo')
    exit()

#PWM.start(channel, duty, freq (default 2000), polarity=0)
PWM.start("P9_14", 50, 31250, 1)

samplemean = range(1,100)
start = range(1,1000); end = range(1,1000)
while True:
    for j in range(0,99):
        for i in range(0,999):
            D = rand.randrange(0, 1)
            start[i] = time.clock()
            PWM.set_duty_cycle("P9_14", D)
            end[i] = time.clock()
        samplemean[j] = (sum(end)-sum(start))/len(end)
    stddev = np.std(samplemean)
    normalmean = sum(samplemean)/len(samplemean)
    print(normalmean, stddev); exit()
# PWM.stop("P9_14")
# PWM.cleanup()
