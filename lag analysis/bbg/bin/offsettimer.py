# Find the time it takes to run time commands 
# Will be subtracted as anx offset from other timer files
import time
import numpy as np

samplemean = range(1,100)
start = range(1,10000); end = range(1,10000)
for j in range(0,99):
    for i in range(0,9999):
        start[i] = time.clock()
        end[i] = time.clock()
    samplemean[j] = (sum(end) - sum(start))/len(end)
sigma = np.std(samplemean)
normalmean = sum(samplemean)/len(samplemean)
print(normalmean, "sigma: ", sigma)
