import time
import os
import sys
import random as rand
import numpy as np

# Run sudo check
if os.getuid() == 0:
    print('Running in sudo')
else:
    print('Error: Run in sudo')
    exit()

def get_power(v,i):
    return v*i

def po_duty(direction, duty):
    if (direction):
        duty = min(duty+step,1)
    else:
        duty = max(duty-step,0)
    return duty

p_last = 0; count = 0; duty = 0.5; step = 0.025
samplemean = range(1,100)
start = range(1,1000); end = range(1,1000)
while True:
    for j in range(0,99):
        for i in range(0,999):
            v_reading = rand.randrange(0, 1200)
            i_reading = rand.randrange(0, 1200)
            start[i] = time.clock()
            p_reading = get_power(v_reading, i_reading)
            delta_p = p_last - p_reading
            if delta_p > 0:
                duty = po_duty(True, duty)
            else:
                duty = po_duty(False, duty)
            p_last = p_reading
	    end[i] = time.clock()
        samplemean[j] = (sum(end)-sum(start))/len(end)
    stddev = np.std(samplemean)
    normalmean = sum(samplemean)/len(samplemean)
    print(normalmean, stddev); exit()
    #print(delta_p, "%.4f"%(duty))
    #time.sleep(0.5)
