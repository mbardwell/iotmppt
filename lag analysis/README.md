# README #

### What is this folder for? ###

* Calculating the lag time associated with AWS API calls from a Beaglebone Green and ESP8266 Feather

### How do I get set up? ###

* Summary of set up: folder contains a mix of Python 3 and C++
* Configuration: refer to [AWS IoT documentation](https://www.amazonaws.cn/en/iot-platform/)
