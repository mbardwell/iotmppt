#include <Arduino.h>
#include "Statistic.h"

Statistic timeStats;
Statistic timeStats2;

#define PWM_MIN 0
#define PWM_MAX 4095  // assuming 12 bit PWM
#define PWM_STEP  10

float P_reading;
float last_V_reading;
bool increase_pwm = false;
uint16_t current_pwm = PWM_MIN;

float get_power(float V, float I);
bool po_pwm_keep_direction(float p, float p_last);
uint16_t po_pwm();

void setup() {
  timeStats.clear();
  timeStats2.clear();
  Serial.begin(115200);
}

void loop() {
  if (timeStats.count() >= 100)
  {
    timeStats2.add(timeStats.average());
    timeStats.clear();
  }

  if (timeStats2.count() >= 30)
  {
    Serial.print("Average: ");
    Serial.print(timeStats2.average(), 4);
    Serial.print(" unbias stdev: ");
    Serial.println(timeStats2.unbiased_stdev(), 4);
    timeStats2.clear();
  }

  V_reading = float(random(0, 1200))/100;
  I_reading = float(random(0, 1200))/100;

  unsigned long time_start_iter = micros();
  P_reading = get_power(V_reading, I_reading);
  po_pwm();
  last_V_reading = V_reading;
  last_I_reading = I_reading;
  last_P_reading = P_reading;
  unsigned long time_end_iter = micros();
  unsigned long time_iter = time_end_iter - time_start_iter;
  timeStats.add(time_iter);
}

float get_power(float V, float I){
  return (V * I);
}

bool po_pwm_keep_direction(float p, float p_last){
  if(p > p_last){
    return true;
    }
  else{
    return false;
  }
}

uint16_t po_pwm(){
  if(!po_pwm_keep_direction(P_reading, last_P_reading)){
    increase_pwm ^= 1;
  }

  if(increase_pwm){
    current_pwm = min((current_pwm + PWM_STEP), PWM_MAX);
  }
  else{
    current_pwm = max((current_pwm - PWM_STEP), PWM_MIN);
  }
}
