#include <Arduino.h>
#include "Statistic.h"

Statistic timeStats;
Statistic timeStats2;

void setup() {
  timeStats.clear();
  timeStats2.clear();
  Serial.begin(115200);
}

void loop() {
  if (timeStats.count() >= 100)
  {
    timeStats2.add(timeStats.average());
    timeStats.clear();
  }

  if (timeStats2.count() >= 30)
  {
    Serial.print("Average: ");
    Serial.print(timeStats2.average(), 4);
    Serial.print(" unbias stdev: ");
    Serial.println(timeStats2.unbiased_stdev(), 4);
    timeStats2.clear();
  }

  unsigned long time_start = micros();
  analogRead(A0);
  unsigned long time_end = micros();
  unsigned long time_iter = time_end - time_start;
  timeStats.add(time_iter);
}
