#include <Arduino.h>

#define duty_MIN 0
#define duty_MAX 4095  // assuming 12 bit duty
#define duty_STEP  10

float V_reading;
float I_reading;
float P_reading;
float last_V_reading;
float last_I_reading;
float last_P_reading;
bool increase_duty = false;
uint16_t current_duty = 0.5*duty_MAX; // Better to start mid-DC

float get_power(float V, float I);
bool po_duty_keep_direction(float p, float p_last);
uint16_t po_duty();
uint16_t ic_duty();

void setup() {
  Serial.begin(9600);
  Serial.println("Hello World!");

}

void loop() {
  V_reading = float(random(0, 1200))/100;
  I_reading = float(random(0, 1200))/100;
  P_reading = get_power(V_reading, I_reading);

  // po_duty(current_duty);
  ic_duty(current_duty);
  // FOCV_duty(current_duty);

  last_V_reading = V_reading;
  last_I_reading = I_reading;
  last_P_reading = P_reading;


  // Serial.println(float(random(0, 1200))/100);
  Serial.println(current_duty);
  delay(100);
    // put your main code here, to run repeatedly:
}

float get_power(float V, float I){
  return (V * I);
}

float measure_voc(){
  // digitalWrite(RelayPin,LOW); // Open panel circuit
  // v_oc = analogRead(v_oc_pin); // Read voltage across PV array
  // digitalWrite(RelayPin, HIGH); // Close panel circuit
  // return v_oc
}

bool po_duty_keep_direction(float p, float p_last){
  uint16_t tol = 0.001;
  if(p - p_last > tol){
    return true;
    }
  else{
    return false;
  }
}

uint16_t po_duty(current_duty){ // Added pass-through variable
  if(!po_duty_keep_direction(P_reading, last_P_reading)){
    increase_duty ^= 1; // What is ^= ?
  }

  if(increase_duty){
    current_duty = min((current_duty + duty_STEP), duty_MAX);
  }
  else{
    current_duty = max((current_duty - duty_STEP), duty_MIN);
  }
}

uint16_t ic_duty(current_duty){ // Added pass-through variable
  float dV = V_reading - last_V_reading;
  float dI = I_reading - last_I_reading;
  float dP = P_reading - last_P_reading;
  uint16_t tol = 0.001;

  if (round(dV) < tol && round(dV) > -tol ){ // was round(dV)==0
    if (round(dI) > tol || round(dI) < -tol){ // was round(dI)!=0
      if (dI > 0) {
        current_duty = min((current_duty + duty_STEP), duty_MAX);
      }
      else{
        current_duty = max((current_duty - duty_STEP), duty_MIN);
      }
    }
  }
  else{
    if(round(dP/dV) > tol || round(dP/dV) < -tol){ // was round(dP/dV) != 0
      if(dP/dV > 0)
      {
        current_duty = min((current_duty + duty_STEP), duty_MAX);
      }
      else{
        current_duty = max((current_duty - duty_STEP), duty_MIN);
      }
    }
  }
}

// uint16_t FOCV_duty(current_duty){
//   float Koc = 0.8;
//   v_mpp = Koc*measure_voc();
//   current_duty = duty_setting_function(v_mpp)
// }
