#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>
#include "Statistic.h"
#include "config/ConnectionParams.h"
#include "mqtt/MqttClient.h"
#include "aws/AwsIotSigv4.h"
#include "aws/ESP8266DateTimeProvider.h"

const char *AP_SSID = "***";
const char *AP_PASS = "***";

ESP8266DateTimeProvider dtp;
AwsIotSigv4 sigv4(&dtp);
ConnectionParams cp(sigv4);
WebSocketClientAdapter adapter(cp);
MqttClient client(adapter, cp);

Statistic timeStats;
Statistic timeStats2;

uint32_t time_sent;

void setup() {
  timeStats.clear();
  timeStats2.clear();
  Serial.begin(115200);
  while(!Serial) {
      yield();
  }

  WiFi.begin(AP_SSID, AP_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  int res = client.connect();
  Serial.printf("mqtt connect=%d\n", res);

  if (res == 0) {
    client.subscribe("t1", 1,
      [](const char* topic, const char* msg)
      {
        uint32_t time_received = micros();
        uint32_t time_transport = time_received - time_sent;
        timeStats.add(time_transport);
        // Serial.print(timeStats.count());
        // Serial.print(", ");
        // Serial.println(timeStats2.count());
      }
    );
  }
}

void loop() {
  if (timeStats.count() >= 100)
  {
    timeStats2.add(timeStats.average());
    timeStats.clear();
  }
  if (timeStats2.count() >= 30)
  {
    Serial.print("Average: ");
    Serial.print(timeStats2.average(), 4);
    Serial.print(" unbias stdev: ");
    Serial.println(timeStats2.unbiased_stdev(), 4);
    timeStats2.clear();
  }
  if (client.isConnected()) {
    time_sent = micros();
    client.publish("t1", "4206969", 0, false);
    client.yield();
  } else {
    Serial.println("Not connected...");
    delay(2000);
  }
}
