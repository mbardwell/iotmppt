#include <Arduino.h>

#define PWM_MIN 0
#define PWM_MAX 4095  // assuming 12 bit PWM
#define PWM_STEP  10

float V_reading;
float I_reading;
float P_reading;
float last_V_reading;
float last_I_reading;
float last_P_reading;
bool increase_pwm = false;
uint16_t current_pwm = PWM_MIN;

bool start_counting = true;
const uint16_t counts = 1000;
uint16_t current_iter = 0;
unsigned long lots_of_time = 0;


float get_power(float V, float I);
bool po_pwm_keep_direction(float p, float p_last);
uint16_t po_pwm();
uint16_t ic_pwm();

void setup() {
  Serial.begin(9600);
  Serial.println("Hello World!");
    // put your setup code here, to run once:
}

void loop() {
  if(start_counting){
    current_iter = 0;
    lots_of_time = 0;
    start_counting = false;
  }

  V_reading = float(random(0, 1200))/100;
  I_reading = float(random(0, 1200))/100;
  P_reading = get_power(V_reading, I_reading);

  unsigned long time_start_iter = micros();
      // po_pwm();
        ic_pwm();
  unsigned long time_end_iter = micros();
  unsigned long time_iter = time_end_iter - time_start_iter;
  lots_of_time += time_iter;

  last_V_reading = V_reading;
  last_I_reading = I_reading;
  last_P_reading = P_reading;

  current_iter += 1;
  // Serial.println(current_iter);
  delay(10);

  if(current_iter == counts){
    start_counting = true;
    Serial.print("TIME: ");
    Serial.print(lots_of_time);
    Serial.println(" uS");
  }
}

float get_power(float V, float I){
  return (V * I);
}

bool po_pwm_keep_direction(float p, float p_last){
  if(p > p_last){
    return true;
    }
  else{
    return false;
  }
}

uint16_t po_pwm(){
  if(!po_pwm_keep_direction(P_reading, last_P_reading)){
    increase_pwm ^= 1;
  }

  if(increase_pwm){
    current_pwm = min((current_pwm + PWM_STEP), PWM_MAX);
  }
  else{
    current_pwm = max((current_pwm - PWM_STEP), PWM_MIN);
  }
}

uint16_t ic_pwm(){
  float dV = V_reading - last_V_reading;
  float dI = I_reading - last_I_reading;
  float dP = P_reading - last_P_reading;

  if (round(dV) == 0){
    if (round(dI) != 0){
      if (dI > 0) {
        current_pwm = min((current_pwm + PWM_STEP), PWM_MAX);
      }
      else{
        current_pwm = max((current_pwm - PWM_STEP), PWM_MIN);
      }
    }
  }
  else{
    if(round(dP/dV) != 0){
      if(dP/dV > 0)
      {
        current_pwm = min((current_pwm + PWM_STEP), PWM_MAX);
      }
      else{
        current_pwm = max((current_pwm - PWM_STEP), PWM_MIN);
      }
    }
  }
}
