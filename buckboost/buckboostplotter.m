% Assuming you have run buckboost.slx
% Uses outputs from "to workspace" block
% Mike Bardwell, University of Alberta, MSc. Candidate

%% Extract simulation values
clc;
t = buckboost.time;
x = buckboost.signals.values(:,1);
Vpv = buckboost.signals.values(:,2);
Ipv = buckboost.signals.values(:,3);
Id = buckboost.signals.values(:,4);
G = buckboost.signals.values(:,5);
T = buckboost.signals.values(:,6);

%% Calculations
Ppv = Vpv.*Ipv;

%% Plot
subplot(3,1,1); plot(t,Ppv); ylabel('Extracted PV Power (W)')
subplot(3,1,2); plot(t,x); ylabel('Perturbation Variable')
subplot(3,1,3); plot(t,G); xlabel('Time (s)'); ylabel('Irradiance (W/m^2)')

subplot(3,1,1); title('Buckboost Converter With MPPT Performance')