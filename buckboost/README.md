# README #

### What is this folder for? ###

* Simulating buckboost converter maximum power point tracking operation
* All files included are attempts at MPPT control with a non-linear (buckboost) converter. They are still a work in progress


### How do I get set up? ###

* Summary of set up: folder contains a mix of MATLAB and Simulink
* Configuration: use MATLAB R2017b or up for simulink files. Some only work with R2018a or up. R2018a has useful PV examples not found in older versions
* TO DO: Some of the scripts in ../utils may be necessary to run buckboost.slx. They need to be put in a subfolder ./+Scripts
