# README #

### What is this folder for? ###

* Simulating buck converter maximum power point tracking operation
* The simulations used in the papers are labelled accordingly. Some files included are still a work in progress

### How do I get set up? ###

* Summary of set up: folder contains a mix of MATLAB and Simulink
* Configuration: use MATLAB R2017b or up for simulink files. Some only work with R2018a or up. R2018a has useful PV examples not found in older versions

### Buck converter Calculations ###

* TO DO: Clean

* Input variables: dmax = 0.6; dmin = 0.0027; dinit = 0.27; vinit = 25; pinit = 200; %x = vinit; dx = 0.025;

* Selecting passive values for CCM: f = 32150Hz, Vinmin = 25, Vinmax = 36.3, Voutmin = 9V, Voutmax = 15V, Iout = 1.2 A, Vripple = 5%, dVout = 0.1V

* Passive calculations: 
-Dmin = Vomin/Vimax = 24.8% 
-Dmax = Vomax/Vimin = 60%
-diL = 0.4*Ioutmax = 0.8 A
-L = Vout*(Vin-Vout)/(diL*fs*Vin) = 15*(36.3-15)/(0.8*31250*36.3) = 352 uH
/* above you should graph L as a factor of varying Vin and Vout 
     have a note here about diL */
-L > D*Vin*(1-D)/(f*2*Iout)= 19.697780584918036 uH
/* again, graph L as a factor of D */
-Ipk = (Vinmax*D)/(f*L) = 19.494582059362394 A
-Cout = diL/(8*fs*dVout) = 0.8/(8*31250*0.1) = 32uF

* CCM boundary conditions: Rcrit,min = 27*L/(2*Ts) = 27*19uH*31250Hz/2 = 8.22 ohm
* Perturbation period (Tp) and duty cycle step size (x) determination:

?d > (Vbat-Vmpp)*Vmpp/(16*L*Cin*fs^2*Vbat^2)
?d > (40-29)*29/(16*19uH*50uF*31250^2*40^2)
?d_boost > 0.013
/* again try calculating ?d as a factor of various vbat */
 Tp >= -ln(\eta/2)/(sigma*wn)
Tp >= -ln(0.1/2)/(5*32444)
Tp >= 1.8467e-5

* Extra calculation. Another way to calculate duty cycle step size x:

wn = 1/sqrt(L*Cin) = 1/sqrt(19uH*50uF) = 32444 Hz
sigma = sqrt(L/Cin)/(2*Rmpp) + (Rcin+RL)*sqrt(Cin/L)/2
sigma = sqrt(20uH/50uF)/(2*3.7ohm) + (1mohm+6.2ohm)*sqrt(50uF/20uH)/2
sigma = 4.99
RL ~ 36.4V^2/213.15W = 6.2 ohm

Kph = 0.008 A*m^2/W [1: pg 59], Gdot = 100 W/(m^2/s) [3], Tp = 1.85e-5 s, H = 0.0836 A/V^2 [1: pg 59] , Rmpp = 29 V / 7.35 = 3.95 ohms, Go (dc gain) = Vbat = 12 [1: pg 29]
?d > sqrt(Vmpp * Kph * Gdot * 	Tp/(H * Vmpp + (1/Rmpp))) / Go
?d > sqrt(29 * 0.008 * 100 * 1.85e-5/(0.0836 * 29 + (1/3.95))) / 12
?d_buck > 0.001055


### Sources ###

* N. Femia, G. Petrone, G. Spagnuolo, and M. Vitelli, Power electronics and control techniques for maximum energy harvesting in photovoltaic systems. CRC press, 2017

* Buck converter design http://www.powerelectronics.com/dc-dc-converters/buck-converter-design-demystified

* Bardwell calculations using Quebec solar data (see weather profiling folder)