clc;

%% Create datetime array
t1 = datetime(2017,01,1,8,0,0); %y,m,d,h,m,s
t2 = datetime(2017,01,1,8,0,10);
t = t1:milliseconds(1):t2;

%% Clear and Sunny day
norm = normpdf(1:length(t),length(t)/2,1400); norm = 1500*norm/max(norm);

% %% Add Initial Irradiance Punch
% warmth = 1000*ones(1,10000);
% offset = 1;
% norm(offset:offset+length(warmth)-1) = norm(offset:offset+length(warmth)-1) + warmth;
% norm(norm < 0) = 0;

%% Add Noise
noise = wgn(1, 1000, 37, 'real');
offset = 2000;
norm(offset:offset+length(noise)-1) = norm(offset:offset+length(noise)-1) + noise;
norm(norm < 0) = 0;

%% Package the output and create a structure
ts.time = [0:0.001:10]';
ts.signals.values = norm';
plot(ts.time, ts.signals.values);

%% Package the output and create a timeseries
% ts = timeseries(norm, datestr(t));
% plot(ts);