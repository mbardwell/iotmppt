%crossValGenTS.m
function [TT TV] = crossValGenTS(t,n,k)
%function [ind ind2] = crossValGenTS(t,n,k)
%crossValGen.m generates the training and verification set for time series
%using a canonical approach
%http://stats.stackexchange.com/questions/14099/using-k-fold-cross-validation-for-time-series-model-selection
%
% Inputs:
% t: time-series data
% n: n-folds
% k: k-th partition is the testing set
%
% Outputs:
% Tt: Training series
% Tv: Testing series

if n<1
    warning('n must be greater than or equal to 1. \n corrected to be 1.');
    n=1;
end

if k>n
    warning('k must be less than or equal to n. \n k corrected to be n.');
    k=n;
end

%generate the cross validation partition indices
%uneven partitioning is performed
%if mod(size(x,1),n)!=0
partitions=n+1;
for i=0:partitions
    index(i+1)=min(ceil(i*size(t,1)/partitions),size(t,1));
end

indT=[1 index(k+1)];
indV=[index(k+1)+1 index(k+2)];

TT=t(indT(1):indT(2),:);
TV=t(indV(1):indV(2),:);
end

%end crossValGenTS.m