%crossValGen.m
function [Xt Yt Xv Yv ind] = crossValGen(x,y,n,k)
%crossValGen.m generates the training and verification set
%The i/o pairs must be pre-shuffled prior to feeding into the function
%
% Inputs:
% x: inputs
% d: target outputs
% n: n-folds
% k: k-th partition is the testing set
%
% Outputs:
% Xt: Training Input
% Yt: Training Target
% Xv: Testing Input
% Yv: Testing Target

if n<1
    warning('n must be greater than or equal to 1. \n corrected to be 1.');
    n=1;
end

if k>n
    warning('k must be less than or equal to n. \n k corrected to be n.');
    k=n;
end

%xdim=size(x,2);
%ydim=size(y,2);
xds=x;
yds=y;
%dataset=[x y];

%generate the cross validation partition indices
%uneven partitioning is performed
%if mod(size(x,1),n)!=0
for i=0:n
    index(i+1)=min(ceil(i*size(x,1)/n),size(x,1));
end

%copying the testing set
Xv=xds(index(k)+1:index(k+1),:);
Yv=yds(index(k)+1:index(k+1),:);

%removing the testing set from training set
xds(index(k)+1:index(k+1),:)=[];
yds(index(k)+1:index(k+1),:)=[];

%saving training set
Xt=xds;
Yt=yds;

%debug: saving the indices of the testing set
ind=[index(k)+1 index(k+1)];

end
%end crossValGen.m
