%glassREC.m
%Recurrent NN time-series forecaster using layrecnet
clear;
%Step='explore';
Step=input('explore or test?\n','s');

data=importdata('Mackey-Glass.dat');
%data=normalizeLin(data);    %normalize data

%Split training and out-of-sample testing data
oosRatio=2/3;   %select the data split ratio
oosIndex=floor(size(data,1)*oosRatio);  %find the splitting point

trainData=data(1:oosIndex,:);   %training data
oosData=data(oosIndex+1:size(data,1),:);    %out-of-sample testing data

switch Step
    case 'explore'
    tStart=tic;
    
    MAPEA=[];
    MAPEd=[];
    
    MASEA=[];
    MASEd=[];

    NMSEA=[];
    NMSEd=[];
    
    for hiddenLayerSize=1:25
        for feedbackDelays=2:5
            Tt=[];
            Tv=[];
            TT=[];
            TV=[];
            x=[];
            xi=[];
            ai=[];
            t=[];
            xv=[];
            xiv=[];
            aiv=[];
            tv=[];
            
            mape=[];
            mase=[];
            nmse=[];
            
            for cv=1:5          

                [Tt,Tv]=crossValGenTS(trainData,5,cv);
                TT = tonndata(Tt,false,false);
                TV = tonndata(Tv,false,false);

                %set up training data and NN parameters
                trainFcn = 'trainlm';
                net = layrecnet(1:feedbackDelays,hiddenLayerSize,trainFcn);  
                init(net);
                
                [x,xi,ai,t] = preparets(net,TT,TT);
                net.trainParam.showWindow = false;
                net.divideFcn = 'dividetrain';
                net.trainparam.epochs = 1000;
                net.trainparam.goal = 0.005;
                net.divideMode = 'time';
                net.performFcn = 'mse';  % Mean Squared Error

                %train NN
                [net,tr] = train(net,x,t,xi,ai);                

                %calculate performance
                [xv,xiv,aiv,tv] = preparets(net,TV,TV);
                yv=cell2mat(tv);
                Y2=cell2mat(net(xv,xiv,aiv));
                
                mape(cv)=mean(abs((Y2-yv)./yv));
                mase(cv)=mean(abs((Y2-yv)/mean(abs(Y2-mean(yv)))));
                nmse(cv)=mean((Y2-yv).^2)/(mean(Y2)*mean(yv));

            end
            
            MAPEA(hiddenLayerSize,feedbackDelays)=mean(mape);
            MAPEd(hiddenLayerSize,feedbackDelays)=std(mape);
            
            MASEA(hiddenLayerSize,feedbackDelays)=mean(mase);
            MASEd(hiddenLayerSize,feedbackDelays)=std(mase);
            
            NMSEA(hiddenLayerSize,feedbackDelays)=mean(nmse);
            NMSEd(hiddenLayerSize,feedbackDelays)=std(nmse);
        end
    end
    tElap=toc(tStart); 
    fprintf('Parameter exploration completed.\n');
case 'test'
    hiddenLayerSize=6;
    feedbackDelays=3;
    TT = tonndata(trainData,false,false);
    TV = tonndata(oosData,false,false);
    
    trainFcn = 'trainlm';
    net = layrecnet(1:feedbackDelays,hiddenLayerSize,trainFcn);
    init(net);
    [x,xi,ai,t] = preparets(net,TT,TT);
    net.trainParam.showWindow = false;
    net.trainparam.epochs = 1000;
    net.trainparam.goal = 0.005;
    net.divideFcn = 'dividetrain';
    net.divideMode = 'time';
    net.performFcn = 'mse';

    %train NN
    [net,tr] = train(net,x,t,xi,ai);
    
    %calculate performance
    [xv,xiv,aiv,tv] = preparets(net,TV,TV);
    yv=cell2mat(tv);
    YC=cell2mat(net(xv,xiv,aiv));
    
    MAPE=mean(abs((YC-yv)./yv))
    MASE=mean(abs((YC-yv)/mean(abs(YC-mean(yv)))))
    NMSE=mean((YC-yv).^2)/(mean(YC)*mean(yv))
    
    figure(2);
    plot(yv,'linewidth',2);
    hold on;
    plot(YC,'linewidth',2);
    hold off;    
    
otherwise
    fprintf('invalid command \n')
end

%end glassREC.m