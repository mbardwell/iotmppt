%REC.m
%Recurrent NN time-series forecaster using layrecnet
clear;

Step=input('Explore or Test?\n','s');
dataSet=input('Laser or Glass?\n','s');

if strcmpi(dataSet,'laser')
    data=importdata('SantaFeLaserA.dat');
    feedbackDelays=2:5;
else
    data=importdata('Mackey-Glass.dat');
    feedbackDelays=11:15;
end 

if strcmpi(Step,'explore')
    data=normalizeLin(data);    %normalize data    
end        

%Split training and out-of-sample testing data
oosRatio=2/3;   %select the data split ratio
oosIndex=floor(size(data,1)*oosRatio);  %find the splitting point

trainData=data(1:oosIndex,:);   %training data
oosData=data(oosIndex+1:size(data,1),:);    %out-of-sample testing data

switch Step
    case 'explore'
    tStart=tic;
    
    MFEA=[];
    MFEd=[];
    
    MASEA=[];
    MASEd=[];

    NMSEA=[];
    NMSEd=[];
    
    for hiddenLayerSize=1:20
        
        for delay=1:size(feedbackDelays)
            Tt=[];
            Tv=[];
            TT=[];
            TV=[];
            x=[];
            xi=[];
            ai=[];
            t=[];
            xv=[];
            xiv=[];
            aiv=[];
            tv=[];
            
            mfe=[];
            mase=[];
            nmse=[];
            
            for cv=1:5          

                [Tt,Tv]=crossValGenTS(trainData,5,cv);
                TT = tonndata(Tt,false,false);
                TV = tonndata(Tv,false,false);

                %set up training data and NN parameters
                trainFcn = 'trainlm';
                net = layrecnet(1:feedbackDelays(delay),hiddenLayerSize,trainFcn);  
                init(net);
                
                [x,xi,ai,t] = preparets(net,TT,TT);
                %net.trainParam.showWindow = false;
                net.divideFcn = 'dividetrain';
                net.trainparam.epochs = 1000;
                net.trainparam.goal = 0.005;
                net.divideMode = 'time';
                net.performFcn = 'mse';

                %train NN
                [net,tr] = train(net,x,t,xi,ai);                

                %calculate performance
                [xv,xiv,aiv,tv] = preparets(net,TV,TV);
                yv=cell2mat(tv);
                Y2=cell2mat(net(xv,xiv,aiv));
                
                mfe(cv)=mean(Y2-yv);
                mase(cv)=mean(abs((Y2-yv)/mean(abs(Y2-mean(yv)))));
                nmse(cv)=mean((Y2-yv).^2)/(mean(Y2)*mean(yv));
            end
            
            MFEA(hiddenLayerSize,delay)=mean(mfe);
            MFEd(hiddenLayerSize,delay)=std(mfe);
            
            MASEA(hiddenLayerSize,delay)=mean(mase);
            MASEd(hiddenLayerSize,delay)=std(mase);
            
            NMSEA(hiddenLayerSize,delay)=mean(nmse);
            NMSEd(hiddenLayerSize,delay)=std(nmse);
        end
    end
    tElap=toc(tStart); 
    fprintf('Parameter exploration completed.\n');
case 'test'
    if strcmpi(dataSet,'laser')
        hiddenLayerSize=10;
        delay=2;
    elseif strcmpi(dataSet,'glass')
        hiddenLayerSize=11;
        delay=13;
    end
        
    TT = tonndata(trainData,false,false);
    TV = tonndata(oosData,false,false);
    
    trainFcn = 'trainlm';
    net = layrecnet(1:delay,hiddenLayerSize,trainFcn);
    init(net);
    [x,xi,ai,t] = preparets(net,TT,TT);
    %net.trainParam.showWindow = false;
    net.trainparam.epochs = 1000;
    net.trainparam.goal = 0.005;
    net.divideFcn = 'dividetrain';
    net.divideMode = 'time';
    net.performFcn = 'mse';

    %train NN
    [net,tr] = train(net,x,t,xi,ai);
    
    %calculate performance
    [xv,xiv,aiv,tv] = preparets(net,TV,TV);
    yv=cell2mat(tv);
    YC=cell2mat(net(xv,xiv,aiv));
    
    MFE=mean(YC-yv)
    MASE=mean(abs((YC-yv)/mean(abs(YC-mean(yv)))))
    NMSE=mean((YC-yv).^2)/(mean(YC)*mean(yv))
    
    MAE=mean(abs(YC-yv))
    MSE=mean((YC-yv).^2)
    SSE=sum((YC-yv).^2)
    
    figure(2);
    plot(yv,'linewidth',2);
    hold on;
    plot(YC,'linewidth',2);
    hold off;    
    grid on
    
    xlabel('t');
    ylabel('y(t)');
    legend('origional', 'forecasted');
    if strcmpi(dataSet,'laser')
        title('Forecasting Laser Data Set Using Recurrent Neural Network');
    else
        title('Forecasting Mackey-Glass Laser Data Set Using Recurrent Neural Network');
    end
    
otherwise
    fprintf('invalid command \n')
end

%end REC.m

%laserREC2.mat
%glassREC3b.mat