function [Y] = D2D1(tab,removeColumn, removeRow)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
d=tab;
if removeColumn > 0
    d(:,1:removeColumn)=[];
end

if removeRow > 0
    d(1:removeRow,:)=[];
end

d=d';

track=1;
for j=1:size(d,2)
    for i=1:size(d,1)
        Y(track,1)=d(i,j);
        track=track+1;
    end
end

end

