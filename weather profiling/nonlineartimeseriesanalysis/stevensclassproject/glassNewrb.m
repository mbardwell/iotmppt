%glassNewrb.m
%Feed-Forward time-series forecaster using Newrb
clear;

%Step='explore';
Step=input('explore or test?\n','s');

data=importdata('glass_d13_m5.txt');
data=normalizeLin(data);    %normalize data

%Split training and out-of-sample testing data
oosRatio=2/3;   %select the data split ratio
oosIndex=floor(size(data,1)*oosRatio);  %find the splitting point

trainData=data(1:oosIndex,:);   %training data
oosData=data(oosIndex+1:size(data,1),:);    %out-of-sample testing data

switch Step
    case 'explore'
    tStart=tic;
    %Set up parameters to explore
    rb=[1:10 25:25:200];  %Number of hidden nodes to explore
    %rb=[1 25 50];  %Number of hidden nodes to explore
    l=[0.1:0.1:1];  %spread
    
    MAPEA=[];
    MAPEd=[];
    
    MASEA=[];
    MASEd=[];

    NMSEA=[];
    NMSEd=[];

    for s=1:size(l,2)   % for each spread
        d=l(s);
        for r=1:size(rb,2)  %for each node count
            trainSet = trainData(randperm(size(trainData,1)),:);  %shuffle the training data
            
            mape=[];
            mase=[];
            nmse=[];
            
            k=rb(r);
            for cv=1:5 %for each fold
                [xt,yt,xv,yv]=crossValGen(trainSet(:,1:size(trainSet,2)-1),trainSet(:,size(trainSet,2)),10,cv);
                net=newrb(xt', yt', 0, d, rb(r), rb(size(rb,2))+1);    
                Y2=net(xv')';
                
                mape(cv)=mean(abs((Y2-yv)./yv));
                mase(cv)=mean(abs((Y2-yv)/mean(abs(Y2-mean(yv)))));
                nmse(cv)=mean((Y2-yv).^2)/(mean(Y2)*mean(yv));
            end
            
            MAPEA(r,s)=mean(mape);
            MAPEd(r,s)=std(mape);
            
            MASEA(r,s)=mean(mase);
            MASEd(r,s)=std(mase);
            
            NMSEA(r,s)=mean(nmse);
            NMSEd(r,s)=std(nmse);                     
        end
    end    

    MAPEA=[0 l;rb' MAPEA];
    MAPEd=[0 l;rb' MAPEd];
    
    MASEA=[0 l;rb' MASEA];
    MASEd=[0 l;rb' MASEd];

    NMSEA=[0 l;rb' NMSEA];
    NMSEd=[0 l;rb' NMSEd];
    
    tElap=toc(tStart); 
    fprintf('Parameter exploration completed.\n');
    
    case 'test'
        %Train NN using chosen parameters
        k=50;
        d=0.4;
        trainSet = trainData(randperm(size(trainData,1)),:);    %shuffle training set
        xt=trainSet(:,1:size(trainSet,2)-1);
        yt=trainSet(:,size(trainSet,2));
        net=newrb(xt', yt', 0, d, k, k+1); %train network
        
        %Test performance with out-of-sample data
        xv=oosData(:,1:size(oosData,2)-1);
        yv=oosData(:,size(oosData,2));
        Y=net(xv')';
        
        MAPE=mean(abs((Y-yv)./yv))
        MASE=mean(abs((Y-yv)/mean(abs(Y-mean(yv)))))
        NMSE=mean((Y-yv).^2)/(mean(Y)*mean(yv))
        
        figure(2);
        plot(Y,'linewidth',2);
        hold on;
        plot(yv,'linewidth',2);
        hold off;
        
    otherwise
        fprintf('invalid command \n')
end

%end glassNewrb.m