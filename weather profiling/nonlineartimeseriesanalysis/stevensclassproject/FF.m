%FF.m
%Feed-Forward time-series forecaster using Newrb
clear;

Step=input('Explore or Test?\n','s');
dataSet=input('Laser or Glass?\n','s');

if strcmpi(dataSet,'laser')
    data=importdata('laser_d2_m5.txt');
    feedbackDelays=2:5;
else
    data=importdata('glass_d13_m5.txt');
    feedbackDelays=11:15;
end

dmin=min(min(data));
dmax=max(max(data));

data=normalizeLin(data);    %normalize data

%Split training and out-of-sample testing data
oosRatio=2/3;   %select the data split ratio
oosIndex=floor(size(data,1)*oosRatio);  %find the splitting point

trainData=data(1:oosIndex,:);   %training data
oosData=data(oosIndex+1:size(data,1),:);    %out-of-sample testing data

switch Step
    case 'explore'
    tStart=tic;
    %Set up parameters to explore
    rb=[1:10 25:25:200];  %Number of hidden nodes to explore
    %rb=[1 25 50];  %Number of hidden nodes to explore
    l=[0.1:0.1:1];  %spread
    
    MFEA=[];
    MFEd=[];
    
    MASEA=[];
    MASEd=[];

    NMSEA=[];
    NMSEd=[];

    for s=1:size(l,2)   % for each spread
        d=l(s);
        for r=1:size(rb,2)  %for each node count
            trainSet = trainData(randperm(size(trainData,1)),:);  %shuffle the training data
            
            mfe=[];
            mase=[];
            nmse=[];
            
            k=rb(r);
            for cv=1:5 %for each fold
                [xt,yt,xv,yv]=crossValGen(trainSet(:,1:size(trainSet,2)-1),trainSet(:,size(trainSet,2)),10,cv);
                net=newrb(xt', yt', 0, d, rb(r), rb(size(rb,2))+1);    
                Y2=net(xv')';
                
                mfe(cv)=mean(Y2-yv);
                mase(cv)=mean(abs((Y2-yv)/mean(abs(Y2-mean(yv)))));
                nmse(cv)=mean((Y2-yv).^2)/(mean(Y2)*mean(yv));
            end
            
            MFEA(r,s)=mean(mfe);
            MFEd(r,s)=std(mfe);
            
            MASEA(r,s)=mean(mase);
            MASEd(r,s)=std(mase);
            
            NMSEA(r,s)=mean(nmse);
            NMSEd(r,s)=std(nmse);                     
        end
    end    

    MFEA=[0 l;rb' MFEA];
    MFEd=[0 l;rb' MFEd];
    
    MASEA=[0 l;rb' MASEA];
    MASEd=[0 l;rb' MASEd];

    NMSEA=[0 l;rb' NMSEA];
    NMSEd=[0 l;rb' NMSEd];
    
    tElap=toc(tStart); 
    fprintf('Parameter exploration completed.\n');
    
    case 'test'
        %Train NN using chosen parameters
        
        if strcmpi(dataSet,'laser')
            k=25;
            d=0.3;
        elseif strcmpi(dataSet,'glass')
            k=50;
            d=0.4;
        end
                
        trainSet = trainData(randperm(size(trainData,1)),:); %shuffle training set
        xt=trainSet(:,1:size(trainSet,2)-1);
        yt=trainSet(:,size(trainSet,2));
        net=newrb(xt', yt', 0, d, k, k+1); %train network
        
        %Test performance with out-of-sample data
        xv=oosData(:,1:size(oosData,2)-1);
        yv=oosData(:,size(oosData,2));
        Y=net(xv')';
        
        yv2=yv*(dmax-dmin)+dmax;
        Y2=Y*(dmax-dmin)+dmax;
        
        MFE=mean(Y-yv)
        MASE=mean(abs((Y-yv)/mean(abs(Y-mean(yv)))))
        NMSE=mean((Y-yv).^2)/(mean(Y)*mean(yv))
        
        MAE=mean(abs(Y-yv))
        MSE=mean((Y-yv).^2)
        MSE2=mean((Y2-yv2).^2)
        SSE=sum((Y-yv).^2)
        
        figure(2);
        plot(yv,'linewidth',2);
        hold on;       
        plot(Y,'linewidth',2);
        hold off;
        
        grid on
        xlabel('t');
        ylabel('y(t)');
        legend('origional', 'forecasted');
        if strcmpi(dataSet,'laser')
            title('Forecasting Laser Data Set Using Feed-Forward Neural Network');
        else
            title('Forecasting Mackey-Glass Data Set Using Feed-Forward Neural Network');
        end
end

%end FF.m