%normalizeLin.m
function [y] = normalizeLin(x)
maxx=max(x);
minx=min(x);
diffx=maxx-minx;

for j=1:size(x,2)
    for i=1:size(x,1)
        y(i,j)=(x(i,j)-minx(j))/diffx(j);
    end
end

end
%end normalizeLin.m
