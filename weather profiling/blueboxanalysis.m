clc 
% Import data using dragndrop
% For GC data, change timestamp format to HH:mm:ss.SSS. Output type table
% Data for EPEC paper collected from VAR01 very variable/clear sky

datasource = 'BB'; % BB = Blue box, GC = Gov Canada

if strcmp(datasource, 'BB')
    PAR = data.PARPort2;
    PAR = PAR*1000 / max(PAR);
%     t = datetime(data.DateTime);
%     [Prs,trs] = resample(PAR,t,100);
%     diffPAR = diff(Prs(1:1000:end)); diffPAR(end+1) = diffPAR(end);
    diffPAR = diff(PAR);
    stdPAR = std(diffPAR);
    absdiffPAR = abs(diffPAR);
    nozero = absdiffPAR(absdiffPAR>1);
    stdNZPAR = std(nozero);
end

if strcmp(datasource, 'GC')
    g = VAR1.G2Wm2;
    t = VAR1.Timestamphhmmssnnn;
    [grs,trs] = resample(g,t,1); % resamples to 1 s intervals
    diffg = abs(diff(grs)); diffg(end+1) = diffg(end);
    stdg = std(diffg);
    meandiffg = mean(diffg);
    gdotfinalvalue = meandiffg + 3*stdg; % Covers 99.9% of irradiance changes
end