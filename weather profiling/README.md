# README #

### What is this folder for? ###

* Weather analysis provides the average change-in-irradiance-over-time (AKA irradiance slope) value used in perturbation period calculations

### Data Sources ###

* Blue box data was automatically generated from a lab-made atmospheric tracking device placed around the University of Alberta. Contact bardwell@ualberta.ca for more information
* Varennes folders include PDF's with descriptions of the data. The data can be found on the [Natural Resources Canada website](https://www.nrcan.gc.ca/energy/renewable-electricity/solar-photovoltaic/18409)