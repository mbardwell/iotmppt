clc;

D = out.PVstats.signals.values;
T = out.PVstats.time;

D(D<0) = 0; % Set any negative irradiance to 0 (sanity check)

subplot(4,1,1); plot(T, D(:,7))
ylabel('Irradiance (W/m^2)'); title('PV Array Connected to a Boost Converter and Constant Load'); ylim([0 1100])
yyaxis right; plot(T, D(:,8)); ylabel('Temperature (�C)'); legend('Irradiance', 'Temperature')
subplot(4,1,2); plot(T, D(:,1), T, D(:,2), T, D(:,3))
ylabel('Output Power(W)'); lgd = legend('Perturb and Observe', 'Incremental Conductance', 'mixed');
subplot(4,1,3); plot(T, D(:,4), T, D(:,5), T, D(:,6))
xlabel('Time(s)'); ylabel('Duty Cycle'); legend('Perturb and Observe', 'Incremental Conductance', 'mixed')

%% Generate theoretical max power point data
Irradiance = D(:,7);
Temperature = D(:,8)+273.15;
PlotOn = 0;

for i=1:length(T)
    [Vmpp(i), Pmpp(i), Impp(i)] = Scripts.PVCurveGenerator(Irradiance(i), Temperature(i), PlotOn);
end

subplot(4,1,2); hold on; plot(T, Pmpp, '--'); lgd.String{4} = 'Theoretical Power Output'; hold off

%% Efficiency calculations
NoMethods = 2; % P&O and I.C. for now
E = zeros(length(D),NoMethods);
% Pmppfit = cell(3,1); int = cell(3,1);
for j=1:NoMethods
    for i=1:length(D)
        E(i,j) = Pmpp(i)-D(i,j);
        if(E(i,j) < 0)
            E(i,j) = 0;
        end
    end
end

st = 1; stop = length(D);
Tint = T(st:stop);
PmppfitPO = fit(T, D(:,1), 'linearinterp');
intPO = integrate(PmppfitPO, T(stop), T(st));
PmppfitIC = fit(T, D(:,2), 'linearinterp');
intIC = integrate(PmppfitIC, T(stop), T(st));
Pmppfitmax = fit(T, Pmpp(1,:)', 'linearinterp');
intmax = integrate(Pmppfitmax, T(stop), T(st));
nPO = intPO/intmax; nIC = intIC/intmax;

subplot(4,1,4); plot(T, E(:,1), T, E(:,2)); legend('Perturb and Observe', 'Incremental Conductance')
Scripts.vline(T(st)); Scripts.vline(T(stop));

%% (Optional) Generate theoretical max power point data from PV model data
% load('1Soltech 1STH-215-P.mat')
% plot(xdata800W, ydata800W, 'o', xdata1kW, ydata1kW);
% f800 = fit(xdata800W', ydata800W', 'linearinterp');
% f1000 = fit(xdata1kW', ydata1kW', 'linearinterp');
% Pmpp800W = f800(31.3);
% Pmpp1kW = f1000(31.6);
% Pmpp = Pmpp1kW*(D(:,5)==D(20000,5)) + Pmpp800W*(D(:,5)==D(end,5));
% subplot(3,1,2); hold on; plot(T, Pmpp, '--'); hold off%legend('Theoretical Power Output')
