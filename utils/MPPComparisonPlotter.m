clc; clear;

%% MATLAB PV array parameters
np = 1;  % Number of cells in parallel
ns = 1;  % Number of cells in series
nCell = 60; % Number of cells in a module
Vnom = 36.3;  % Open circuit voltage under nominal conditions (V)
Inom = 7.84;  % Short circuit current under nominal conditions (A)
Vmp = 29;  % Maximum power voltage (V) - must be for nominal conditions
Imp = 7.35; % Maximum power current (A) - must be for nominal conditions
KVoc = -0.36099;  % Temperature coefficient of Voc (%/deg. C)
KIsc = 0.102;  % Temperature coefficient of Isc (%/deg. C)

Il = 7.8649;  % Light-generated current (A)
I0 = 2.9259e-10;  % Diode saturation current (A)
p = 0.98117;  % Diode ideality factor
Rsh = 313.3991;  % Shunt resistance (Ohms)
Rs = 0.39383;  % Series resistance (Ohms)

Gnom = 1000;  % Nominal irradiation (W/m^2)
Tnom = 298.15;  % Nominal temperature (K)
q = 1.6022e-19;  % Charge constant
k = 1.3806e-23;  % Boltzmann's constant

%% PV array input parameters
G = [1000 900 800 700 600 500 400 300 200 100];  % Input irradiation (W/m^2)
T = 298.15;  % Input temperature (K)

%% Theoretical values
% Figures saved from plot of PV array on Simulink model
open('ivcurves_model.fig');

h = findobj(gca, 'Type', 'Line');
X = get(h, 'Xdata');
Y = get(h, 'Ydata');

% Extract data from I-V curves
Ith{1} = Y{11};
VIth{1} = X{11};

for i=1:9
    Ith{i + 1} = Y{10 - i};
    VIth{i + 1} = X{10 - i};
end

open('pvcurves_model.fig');

h = findobj(gca, 'Type', 'Line');
X = get(h, 'Xdata');
Y = get(h, 'Ydata');

% Extract data from P-V curves
for i=1:10
    Pth{i} = Y{22- 2*i};
    VPth{i} = X{22 - 2*i};
end

clf; close all;  % Comment this line if you want to see original plots

%% Experimental calculations
Vt = (k*T/q)*p*nCell;
Isc = (Il + KIsc*(T-Tnom))*(G/Gnom);  % Isc under input conditions
Voc = Vt*log(Isc/I0 + 1);  % Voc under input conditions

for i = 1:10
    Vexp{i} = 0:0.01:Voc(i);
end

for i = 1:length(G)
    Iexp{i} = zeros(1, length(Vexp{i}));
    for j = 1:length(Vexp{i})
        lambertVal = lambertw((I0*Rs/(Vt*(1+Rs/Rsh))) * exp((Vexp{i}(j) * (1-Rs/Rsh)/Vt) + (((Isc(i) + I0)*Rs)/(Vt*(1+Rs/Rsh)))));
        Iexp{i}(j) = ((Isc(i) + I0) - (Vexp{i}(j)/Rsh))/(1 + Rs/Rsh) - (Vt/Rs)*lambertVal;
        Pexp{i}(j) = Iexp{i}(j)*Vexp{i}(j);
    end
end

%% Plot all curves
figure(1);
hold on;
for i = 1:length(G)
    exper = plot(Vexp{i}, Iexp{i}, 'r');
    th = plot(VIth{i}, Ith{i}, 'k');
end
ylim([0 1.05*Isc(1)]);
xlabel('Voltage [V]');
ylabel('Current [A]');
title('I-V Curve Comparison');
legend([exper th], 'Experimental', 'Theoretical');
hold off;

figure(2);
hold on;
for i = 1:length(G)
    exper = plot(Vexp{i}, Pexp{i}, 'r');
    th = plot(VPth{i}, Pth{i}, 'k');
end
ylim([0 1.05*max(Pexp{1})]);
xlabel('Voltage [V]');
ylabel('Power [W]');
title('P-V Curve Comparison');
legend([exper th], 'Experimental', 'Theoretical');
hold off;

%% Calculate discrepancies between maximum power points
% Allocate arrays for holding maximum values
PthMax = zeros(1, length(G));
VthMax = zeros(1, length(G));
IthMax = zeros(1, length(G));
PexpMax = zeros(1, length(G));
VexpMax = zeros(1, length(G));
IexpMax = zeros(1, length(G));

% Determine maximum power and I-V combination for each irradiance value
for i = 1:length(G)
    [PthMax(i), index] = max(Pth{i});
    VthMax(i) = VPth{i}(index);
    IthMax(i) = interp1(VIth{i}, Ith{i}, VthMax(i));  % May be slightly off due to interpolation
    [PexpMax(i), index] = max(Pexp{i});
    VexpMax(i) = Vexp{i}(index);
    IexpMax(i) = interp1(Vexp{i}, Iexp{i}, VexpMax(i));
end

%% Plot maximum power points on I-V space
figure(3);
hold on;
scatter(VexpMax, IexpMax, 'r', 'DisplayName', 'Experimental');
scatter(VthMax, IthMax, 'k', 'DisplayName', 'Theoretical');
xlabel('Voltage [V]');
ylabel('Current [A]');
title('Maximum Power Point Distribution in I-V Space');
legend('show');
hold off;

%% Plot maximum power points on P-G space
figure(4);
hold on;
scatter(G, PexpMax, 'r', 'DisplayName', 'Experimental');
scatter(G, PthMax, 'k', 'DisplayName', 'Theoretical');
xlabel('G [W/m^2]');
ylabel('P [W]');
title('Maximum Power Point Distribution in P-G Space');
legend('show');
hold off;