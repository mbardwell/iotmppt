clc; clear
%Currently the data is only set up for Varennes files
%VV = Very variable, CS = Clear sky

%Window data 

data = load('20141230_VAR01_CS.mat');
input(1,:) = data.G1Wm2_rs/max(data.G1Wm2_rs);
input(2,:) = data.G2Wm2_rs/max(data.G2Wm2_rs);

%Only normalize diff if it exceeds 1
if max(diff(input(1,:))) > 1 || max(diff(input(2,:))) > 1
    diffinput(1,:) = diff(input(1,:))/max(diff(input(1,:)));
    diffinput(2,:) = diff(input(2,:))/max(diff(input(2,:)));
else
    diffinput(1,:) = diff(input(1,:));
    diffinput(2,:) = diff(input(2,:));
end
%% Break into windows
windowlength = 200;

for i = 1:length(input)-200
    windowdata(:,i) = input(1,i:i+windowlength-1);
    windowdata1(:,i) = input(2,i:i+windowlength-1);
    windowdata2(:,i) = diffinput(1,i:i+windowlength-1);
    windowdata3(:,i) = diffinput(2,i:i+windowlength-1);
end
windowdata(:,:,2) = windowdata1; clear windowdata1 %faster
windowdata(:,:,3) = windowdata2; clear windowdata2 %faster
windowdata(:,:,4) = windowdata3; clear windowdata3 %faster

set = 1;
for i = 1:floor(length(windowdata)/windowlength)
   correlation(i) = corr(windowdata(:,i,set),windowdata(:,i+200,set));
   correlationdiff(i) = corr(windowdata(:,i,set+2),windowdata(:,i,set+2));
end

% plot(correlation); hold on; plot(correlationdiff)

%% Prepare data for export
inputcs1 = con2seq(windowdata(:,:,1));
inputcs2 = con2seq(windowdata(:,:,2));
inputdiffcs1 = con2seq(windowdata(:,:,3));
inputdiffcs2 = con2seq(windowdata(:,:,4));
%Build target cells below. Zeros for CS, Ones for VV
tarinputcs1 = con2seq(zeros(1,length(inputcs1)));
tarinputcs2 = con2seq(zeros(1,length(inputcs2)));
tardiffinputcs1 = con2seq(zeros(1,length(inputdiffcs1)));
tardiffinputcs2 = con2seq(zeros(1,length(inputdiffcs2)));