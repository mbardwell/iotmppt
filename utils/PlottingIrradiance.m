clc; clear

%% Data Preprocessing
NoMonths = 1; % Number of months of data to read
MonthOffset = 6;

for i = 1:NoMonths
    x = num2str(i+MonthOffset);
    if length(i) < 10
        x = strcat('0',x);
    end
    filename = strcat('2017', x, '.csv');
    data = xlsread(filename); % Read in data
end

NoWeeks = 2; % Number of weeks of the data you want to input
bins = zeros(168, NoWeeks);
j = 1; k = 1;

while(j < 169)
   for i = j:168:NoWeeks*168
        bins(j,k) = data(i,3);
        k = k + 1;
   end
   j = j + 1;
   k = 1;
end

% %% Run simulation
NoSamples = NoWeeks*168;
Runtime = 1.2; % Simulation runtime in seconds
Ts_Irradiance = Runtime/NoSamples;
Irradiance.time = (0:Ts_Irradiance:Runtime-Ts_Irradiance)';
Irradiance.signals.values = data(1:NoSamples,3);
Irradiance.signals.dimensions = 1;

load_system('power_PVarray_3500W')
cs = getActiveConfigSet('power_PVarray_3500W');
model_cs = cs.copy;
simOut = sim('power_PVarray_3500W', model_cs);
D = simOut.DCref.signals.values;
T = simOut.DCref.time;
P = simOut.P_panel.signals.values;

%% Plot Results
subplot(3,1,1)
yyaxis left
plot(T, D);
ylabel('Reference for DC Link Voltage (?)');

yyaxis right
plot(Irradiance.time, Irradiance.signals.values);
ylabel('Irradiance Data (W/m^2)');
xlabel('Time (s)');
title('Output of P&O Algorithm Versus Irradiance Data');

subplot(3,1,2)
title('Output of P&O Algorithm Versus Panel Power Output');
xlim([T(floor(NoSamples/2)), T(length(T))]);
xlabel('Time (s)');
yyaxis left
plot(T(floor(NoSamples/2):end), D(floor(NoSamples/2):end)/max(D(floor(NoSamples/2):end)));
ylabel('Reference for DC Link Voltage (?)');

yyaxis right
plot(T(floor(NoSamples/2):end-1), P(floor(NoSamples/2):end-1));
ylabel('Grid Power Flow (kW)');

subplot(3,1,3)
title('Grid Stats');
xlabel('Time (s)');
yyaxis left
plot(simOut.VIgrid.time, simOut.VIgrid.signals(1).values);
ylabel('Grid Voltage');

yyaxis right
plot(simOut.VIgrid.time, simOut.VIgrid.signals(2).values);
ylabel('Grid Power');