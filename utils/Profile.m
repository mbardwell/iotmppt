P = I.*V; P1 = I1.*V1;
plot(V,P); hold on; plot(Vmp, Pmax, 'o')
plot(V1,P1); plot(V1mp, P1max, '*')

xlabel('Voltage (V)')
ylabel('Power (W)')
title('P-V Characteristics of 3 Series 1Soltech 1STH-215-P Modules')
axis([V(1) V(end) 0 round(Pmax*1.1)])
legend('1000 W', '1000 W Mpp', '800 W', '800 W Mpp')