function [xdata, ydata] = FigDataExtraction(figname)
    open(strcat(figname, '.fig'));
    h = gcf; %current figure handle
    
    axesObjs = get(h, 'Children');  %axes handles
    
    h = findobj(gca,'Type','line');
    xdata = get(h,'Xdata');
    ydata = get(h,'Ydata');
    
    %Need to add a feature that extracts curve data into a matrix
    %Currently I can only extract from subplot 2, need to add subplot 1
end