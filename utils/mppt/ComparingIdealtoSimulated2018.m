clc;

D = PVstats.signals.values; % out.PVstats.signals.values;
T = PVstats.time; % out.PVstats.time;
D = D(find(T==1.5):end,:);
T = T(find(T==1.5):end);

D(D<0) = 0; % Set any negative irradiance to 0 (sanity check)
if MPPTMode == 1; MPPTMethod = 'Fractional Voltage'; end
if MPPTMode == 2; MPPTMethod = 'Perturb and Observe'; end
if MPPTMode == 3; MPPTMethod = 'Incremental Conductance'; end

subplot(4,1,1); plot(T, D(:,7))
ylabel('Irradiance (W/m^2)'); legend('Irradiance');
title('PV Array Connected to a Boost Converter and Constant Load');
subplot(4,1,2); plot(T, D(:,5))
ylabel('Output Power(W)'); lgd = legend('placeholder');
subplot(4,1,4); plot(T, D(:,8))
ylabel('Duty Cycle'); lgd = legend('placeholder');

%% Generate theoretical max power point data
load('LUT.mat')
D(D(:,7) <= 0.51, 7) = 1;
Irradiance = D(:,7);
Pmax = Pmpp(round(Irradiance));

% for i=1:length(T)
%     [Vmpp(i), Pmpp(i), Impp(i)] = Scripts.PVCurveGenerator2018(Irradiance(i), 0);
% end

subplot(4,1,2); hold on; plot(T, Pmax, '--'); lgd.String{4} = 'Theoretical'; hold off

%% Efficiency Calculations Using Lookup Table
Error = Pmax - D(:,5);
% Error(Error > 0.2*max(D(:,5))) = 0; Error(Error < 0) = 0; % remove transient error
subplot(4,1,3); plot(T, Error);
ylabel('Error (W)'); lgd = legend(strcat('placeholder', ' Error'));

%% Efficiency calculations
% lag = 20000;
% interror = zeros(length(T),1); interror(1:lag) = NaN;
% mppfit = fit(T,D(:,5), 'linearinterp');
% for i = lag+1:length(T)
%     interror(i) = integrate(mppfit, i, i+lag);
% end
% subplot(4,1,3); hold on; plot(T, interror, 'o');
% NoMethods = 2; % P&O and I.C. for now
% E = zeros(length(D),NoMethods);
% Pmppfit = cell(3,1); int = cell(3,1);
% for j=1:NoMethods
%     for i=1:length(D)
%         E(i,j) = Pmpp(i)-D(i,j);
%         if(E(i,j) < 0)
%             E(i,j) = 0;
%         end
%     end
% end
% 
% st = 1; stop = length(D)
% Tint = T(st:stop);
% PmppfitPO = fit(T, D(:,1), 'linearinterp');
% intPO = integrate(PmppfitPO, T(stop), T(st));
% PmppfitIC = fit(T, D(:,2), 'linearinterp');
% intIC = integrate(PmppfitIC, T(stop), T(st));
% Pmppfitmax = fit(T, Pmpp(1,:)', 'linearinterp');
% intmax = integrate(Pmppfitmax, T(stop), T(st));
% nPO = intPO/intmax; nIC = intIC/intmax;
% 
% subplot(3,1,3); plot(T, E(:,1), T, E(:,2)); legend('Perturb and Observe', 'Incremental Conductance')
% Scripts.vline(T(st)); Scripts.vline(T(stop));

%% (Optional) Generate theoretical max power point data from PV model data
% load('1Soltech 1STH-215-P.mat')
% plot(xdata800W, ydata800W, 'o', xdata1kW, ydata1kW);
% f800 = fit(xdata800W', ydata800W', 'linearinterp');
% f1000 = fit(xdata1kW', ydata1kW', 'linearinterp');
% Pmpp800W = f800(31.3);
% Pmpp1kW = f1000(31.6);
% Pmpp = Pmpp1kW*(D(:,5)==D(20000,5)) + Pmpp800W*(D(:,5)==D(end,5));
% subplot(3,1,2); hold on; plot(T, Pmpp, '--'); hold off%legend('Theoretical Power Output')
