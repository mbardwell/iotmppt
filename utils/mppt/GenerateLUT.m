clc;
Vmpp = zeros(2000,1);
Pmpp = zeros(2000,1);
Impp = zeros(2000,1);

for i=1:2000
    [Vmpp(i), Pmpp(i), Impp(i)] = Scripts.PVCurveGenerator2018(i,0);
end