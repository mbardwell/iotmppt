% Run this before you run simulink

% Purpose of preprocessing data is to squish information into a 0.1 second
% window. The solver's minimum step size is 'auto', so I believe it can go
% down into 10^-9s/step or 1e8 steps

% Import data using drag and drop method to save time
% resample to 1 Hz -> sanity check for negative irr
% place data into a timeseries

irr = [G1Wm2, G2Wm2];
time = Timestamphhmmssnnn;

[y,ty] = resample(irr, time, 1);

y(y<0) = 0;

% Plot series 1 or 2?
series = 1;
ts = timeseries(y(:,1), datestr(ty));