function [Vmpp, Pmpp, Impp] = PVCurveGenerator(G, T, plotyn)

    %% MATLAB PV array parameters

    % Fill in panel specifics for diode model. Grouping them like the MATLAB
    % PV Array block does

    nser = 360; % Number of cells in series in a module
    npar = 2; % Number of cells in parallel
    Vnom = 36.3;  % Open circuit voltage under nominal conditions (V)
    Inom = 7.84;  % Short circuit current under nominal conditions (A)
    Vmp = 29;  % Maximum power voltage (V) - must be for nominal conditions
    Imp = 7.35; % Maximum power current (A) - must be for nominal conditions
    KVoc = -0.36099;  % Temperature coefficient of Voc (%/deg. C)
    KIsc = 0.102;  % Temperature coefficient of Isc (%/deg. C)

    Il = 7.8649;  % Light-generated current (A)
    I0 = 2.9273e-10;  % Diode saturation current (A)
    p = 0.98117;  % Diode ideality factor
    Rsh = 313.3991;  % Shunt resistance (Ohms)
    Rs = 0.39383;  % Series resistance (Ohms)

    Gnom = 1000;  % Nominal irradiation (W/m^2)
    Tnom = 298.15;  % Nominal temperature (K)
    q = 1.60217662e-19;  % Charge constant
    k = 1.38064852e-23;  % Boltzmann's constant

    %% Plotting I-V curve
    
%     G = 1000;  % Input irradiation (W/m^2)
%     T = 298.15;  % Input temperature (K)
    
    Vt = (p*k*T*nser/q);
    Isc = (Il + KIsc*(T-Tnom)).*(G/Gnom);  % Isc under input conditions
    
    Voc = Vt*log(Isc/I0 + 1);  % Voc under input conditions
    Vd = 0:0.01:ceil(Voc);

    I = zeros(1, length(Vd));
    for j = 1:length(Vd)
        lambertVal = lambertw((I0*Rs/(Vt*(1+Rs/Rsh))) * exp((Vd(j) * (1-Rs/Rsh)/Vt) + (((Isc + I0)*Rs)/(Vt*(1+Rs/Rsh)))));
        I(j) = ((Isc + I0) - (Vd(j)/Rsh))/(1 + Rs/Rsh) - (Vt/Rs)*lambertVal;
    end
    
%     I = npar*(Isc - I0 * (exp(Vd./Vt) - 1) - Vd./Rsh); % No Rs?
%     I = npar*(Isc - I0 * (exp(Vd./Vt) - 1))*Rsh/(Rsh + Rs) - Vd/(Rs + Rsh);  
    
    P = I.*Vd;
    Pmpp = max(P);
    Impp = max(I);
    Vmpp = Vd(P == Pmpp);
        
    if(plotyn)
        fprintf('Isc: %.2f\n', Isc);
        fprintf('Voc: %.2f\n', Voc);
        
%         figure(2);
        subplot(2,1,1)
        plot(Vd, I, 'r');
        xlabel('Voltage [V]');
        ylabel('Current [A]');
        title('PV Array I-V curve');
        ylim([0 1.05*Isc]);

        subplot(2,1,2)
        plot(Vd, P, 'b');
        xlabel('Voltage [V]');
        ylabel('Power [W]');
        title('PV Array P-V curve');
        ylim([0, 1.05*max(P)]);
    end
end