clc; clear

load('CSinputsandtargets.mat');
load('VVinputsandtargets.mat');

%% Mix datasets
input = inputcs1;
target = tarinputcs1;

for i=1:length(inputcs1)
    input{end+1} = inputvv1{i};
    target{end+1} = tarinputvv1{i};
end

perm = randperm(length(input));
inputmix = input(perm);
targetmix = target(perm);
trainingsize = 1:2000;

%% Build net
d1 = [1:1];
d2 = [1:2];
narx_net = narxnet(d1,d2,10);
narx_net.divideFcn = '';
narx_net.trainParam.min_grad = 1e-10;
narx_net.layers{2}.transferFcn = 'logsig'; % tried hardlim, logsig
[p,Pi,Ai,t] = preparets(narx_net,inputmix,{},targetmix);

%% Train series-parallel open loop net
narx_net = train(narx_net,p,t,Pi);
%Sim results
yp = sim(narx_net,p,Pi);
e = cell2mat(yp)-cell2mat(t);
subplot(2,1,1); plot(e); title('series-parallel open loop net performance')

%% Convert trained network to closed loop for multistep ahead prediction
narx_net_closed = closeloop(narx_net);
view(narx_net)
view(narx_net_closed)

%% Perform multistep ahead prediction
Preditiongap = 200;
y1 = targetmix(length(trainingsize):length(trainingsize)+Preditiongap);
u1 = inputmix(length(trainingsize):length(trainingsize)+Preditiongap);
[p1,Pi1,Ai1,t1] = preparets(narx_net_closed,u1,{},y1);
yp1 = narx_net_closed(p1,Pi1,Ai1);
TS = size(t1,2);
subplot(2,1,2); plot(1:TS,cell2mat(t1),'b',1:TS,cell2mat(yp1),'r', 1:TS,cell2mat(y1(1:199)),'g'); title('Multistep prediction')