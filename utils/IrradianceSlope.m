clc

%% Plotting
t = G1Ty;
dt = t(2:end);
dG1 = diff(G1Wm2_rs);
abs_dG1 = abs(dG1);
% plot(dt, abs_dG1); hold on

range = 10000:50000;
stat_dt = dt(range);
stat_dG1 = abs_dG1(range);
plot(stat_dt, stat_dG1)
xlabel('Military Time')
ylabel('\partialG/\partialt (W/s*m^2)')
title('Time Derivative of Varennes Irradiance Data Collected by CanmetENERGY Resampled at 1 Hz')

%% Stats
avg = mean(stat_dG1); disp(avg)
stddev = std(stat_dG1); disp(stddev)
outlier = avg+3*stddev; disp(outlier)