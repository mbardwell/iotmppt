clc; clear

load('1Soltech 1STH-215-P.mat')
Scripts.PVCurveGenerator(1000, 298.15, 1); % Input irradiance(W/m^2) and Temp(K)
hold on; Scripts.PVCurveGenerator(800, 298.15, 1);
plot(xdata800W, ydata800W, 'o', xdata1kW, ydata1kW, '-o')
legend('Generated curve', 'Generated curve', 'Theoretical 1', 'Theoretical 2')
axis auto; ylim([0 250])