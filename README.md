# README #

### What is this repository for? ###

* Proving the feasible integration of internet of things with energy management systems
* Providing the work behind the following publications

M. Bardwell, J. Wong, S. Zhang, P. Musilek. IoT-based MPPT Controller for Photovoltaic Array. 
In *2018 IEEE Electric Power and Energy Conference (EPEC)*, 2018

M. Bardwell, J. Wong, S. Zhang, P. Musilek. Design Considerations for IoT-based PV Charge Controllers. 
In *IEEE Services 2018 (ICIOT)*, 2018

### Folders ###
Folders are divided by converter topology, supporting scripts and publishings:

* buck converter topology
* boost converter topology
* buckboost converter topology
* lag analysis
* papers: published conference papers
* utils: for idea trials, plotting code and PV curve generation code
* weather profiling: this contains irradiance slope analysis used in the papers

### How do I get set up? ###

* Summary of set up: repo contains a mix of Python 3, C++, MATLAB and Simulink
* Configuration: use MATLAB R2017b or up for simulink files. Some only work with R2018a or up. R2018a has useful PV examples not found in older versions

### Contribution guidelines ###

* Repo too premature for guidelines

### Who do I talk to? ###

* Repo admin: please send emails with title 'OS MPPT IoT Repo - *question/comment*' to bardwell@ualberta.ca